from django.contrib import admin
from rest_framework.authtoken.models import Token

# Do not show Tokens admin
admin.site.unregister(Token)
