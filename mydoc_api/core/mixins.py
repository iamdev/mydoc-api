import uuid

from django.contrib.gis.db import models
from django.contrib.postgres.fields import JSONField
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class BaseMixin(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    metadata = JSONField(
        blank=True,
        default=dict(),
        verbose_name=_('metadata')
    )
    created = models.DateTimeField(auto_now_add=True,
                                   editable=False,
                                   verbose_name=_('date and time of creation'))
    updated = models.DateTimeField(
        auto_now=True,
        editable=False,
        verbose_name=_('date and time of last update')
    )

    class Meta:
        abstract = True


class SoftDeletableMixin(models.Model):
    deleted = models.DateTimeField(blank=True, null=True, db_index=True)

    def delete(self, actually_delete=False):
        if actually_delete:
            super(SoftDeletableMixin, self).delete()
        else:
            myself = self.__class__.objects.filter(pk=self.pk)
            myself.update(deleted=timezone.now())

    def undelete(self):
        self.delete_date = None
        self.save()

    class Meta:
        abstract = True


class AddressMixin(models.Model):
    # Address fields
    street = models.CharField(
        blank=True,
        max_length=128,
        null=True,
        verbose_name=_('street address')
    )

    zipcode = models.CharField(
        blank=True,
        max_length=5,
        null=True,
        verbose_name=_('zip code')
    )
    state = models.CharField(
        blank=True,
        max_length=3,
        null=True,
        verbose_name=_('state')
    )

    country = models.CharField(
        default='Colombia',
        max_length=64,
        verbose_name=_('country')
    )

    def get_full_address(self):
        street = ", ".join(
            item for item in [self.street]
            if item
        )
        state = ", ".join(
            item for item in [self.zipcode, self.state]
            if item
        )
        full_address = "\n".join(
            item for item in [street, state, self.country]
            if item
        )

        return full_address

    class Meta:
        abstract = True
